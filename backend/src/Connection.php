<?php

namespace App;

use PDO;

class Connection
{
    protected PDO $dbConnection;

    public function __construct()
    {
        $config = (include __DIR__ . '/../config/config.php')['db'];
        $this->dbConnection = new PDO(
            'mysql:host=' . $config['host'] . ';dbname=' . $config['dbname'],
            $config['user'],
            $config['password'],
        );
    }

    /**
     * @param string $sql
     * @param mixed $class
     *
     * @return array
     */
    public function connectionAction(string $sql, $class): array
    {
        $connect = $this->dbConnection->prepare($sql);
        $connect->execute();
        $data = $connect->fetchAll();
        $res = [];
        foreach ($data as $row) {
            $item = new $class;
            foreach ($row as $key => $val) {
                if (!property_exists($item, $key)) {
                    continue;
                }
                $item->$key = $val;
            }
            $res[] = $item;
        }

        return $res;
    }

    /**
     * @param string $email
     * @param string $sql
     * @param $class
     *
     * @return mixed
     */
    public function getByEmail(string $email, string $sql, $class)
    {
        $connect = $this->dbConnection->prepare($sql);
        if (!$connect) {
            die("Ошибка при подготовке запроса: " . $this->dbConnection->errorInfo());
        }

        $connect->bindParam("s", $email);

        if ($connect->execute()) {
            $data = $connect->fetch();
            $item = new $class;
            foreach ($data as $key => $val) {
                if (!property_exists($item, $key)) {
                    continue;
                }
                $item->$key = $val;
            }

            return $item;
        } else {
            die("Ошибка при подготовке запроса: " . $this->dbConnection->errorInfo());
        }
    }
}