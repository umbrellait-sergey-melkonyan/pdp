<?php


namespace App;


abstract class Model
{
    public const TABLE = '';

    public int $id;

    public static function findAll(): array
    {
        $db = new Connection();
        $sql = 'SELECT * FROM '. static::TABLE;
        return $db->connectionAction($sql, static::class);
    }
}