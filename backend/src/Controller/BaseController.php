<?php

namespace App\Controller;

/**
 * Class BaseController
 */
abstract class BaseController
{
    /**
     * @param string $template
     */
    public function display(string $template): void
    {
        include dirname(__DIR__) . $template;
    }

    /**
     * @param string $template
     *
     * @return null|string
     */
    public function render(string $template): ?string
    {
        ob_start();
        include dirname(__DIR__) . $template;
        $contents = ob_get_contents();
        ob_end_clean();

        return $contents;
    }
}