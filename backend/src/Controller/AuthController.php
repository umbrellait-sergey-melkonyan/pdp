<?php

namespace App\Controller;

use App\Framework\Http\Request;
use App\Framework\Http\Responses\HtmlResponse;
use App\Models\User;
use Firebase\JWT\JWT;

class AuthController extends BaseController
{
    public function login(Request $request)
    {
        session_start();
        ob_start();

        if ($request->getMethod() === 'POST') {
            $username = trim($_POST['username']);
            $password = trim($_POST['password']);

            if (
                !$username ||
                !filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING) ||
                !filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING)
            ) {
                http_response_code(401);
                exit();
            }

            $user = User::getByEmail($username);

            if ($user && password_verify($password, $user->password)) {
                $_SESSION['username'] = $username;
                $_SESSION['logged_in'] = true;

                $config = (include __DIR__ . '/../config/config.php');
                $secret_key = $config['secret_key'];

                try {
                    $privateKey = base64_encode(sodium_crypto_sign_secretkey($secret_key));
                } catch (\SodiumException $e) {
                    dd('Проблемы хозяйн');
                }

                $token = [
                    "iss" => "example.com",
                    "aud" => "example.com",
                    "iat" => time(),
                    "exp" => time() + 3600, // токен действителен 1 час
                    "username" => $username
                ];

                $jwt = JWT::encode($token, $privateKey, 'EdDSA');

                setcookie('token', $jwt, time() + 60*60*24);
                // и тут еще нужно записать в local storage

                //отдавал токен в теле ответа
//                header('Authorization: Bearer '.$jwt);
//                http_response_code(200);
                header('Location:/');
                exit;
            } else {
                http_response_code(401);
                exit();
            }
        } else {
            dd('косяк');
        }
    }

    public function singUp(): HtmlResponse
    {
        return new HtmlResponse($this->render('/Templates/Auth/login.php'));
    }

    public function logout()
    {
        session_unset();
        session_destroy();
        header('Location: /');
        exit;
    }



}