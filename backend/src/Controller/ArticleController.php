<?php


namespace App\Controller;

use App\Framework\Http\Request;
use App\Framework\Http\Responses\JsonResponse;
use App\Framework\Http\Responses\Response;
use App\Service\ArticleService;

/**
 * Class ArticleController
 */
class ArticleController
{
    private ArticleService $articleService;

    /**
     * ArticleController constructor.
     *
     * @param ArticleService $articleService
     *
     * @throws
     */
    public function __construct(ArticleService $articleService)
    {
        $this->articleService = $articleService;
    }

    /**
     * @return JsonResponse
     */
    public function getListAction(): JsonResponse
    {
        $data = $this->articleService->getData();

        return new JsonResponse($data);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getArticleById(Request $request): JsonResponse
    {
        $id = $request->getAttribute('id');

        $data = $this->articleService->getData();

        if (!$id) {
            new JsonResponse(['error' => 'Undefined page1'], Response::HTTP_NOT_FOUND);
        }

        if (isset($data[$id-1])) {
            return new JsonResponse($data[$id-1]);
        }

        return new JsonResponse(['error' => 'Undefined page2'], Response::HTTP_NOT_FOUND);
    }

}