<?php


namespace App\Controller;

use App\Framework\Http\Responses\HtmlResponse;
use App\Models\Article;

/**
 * Class DataController
 */
class DataController extends BaseController
{
    public function action()
    {

        $data = (new Article())->find();
        $this->data = $data;
        $this->render('/Templates/index.php');
        return new HtmlResponse($this->render('/Templates/index.php'));
    }


}