<?php
declare(strict_types=1);

namespace App\Controller;

use App\Framework\Http\Responses\HtmlResponse;
use App\Models\Article;

/**
 * Class HelloWorld
 */
class HelloWorld extends BaseController
{
    public function action(): HtmlResponse
    {
//        $this->data = Article::findAll();
        return new HtmlResponse($this->render('/Templates/index.php'));
    }
}
