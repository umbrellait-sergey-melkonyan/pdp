<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>PHP</title>
</head>
<body>
<h3>Hello world! Статьи</h3>
<h3><a href="/logout">logout</a></h3>
<?php //dd( $this->data) ?>
<table>
  <thead>
  <tr>
    <th scope="col">#</th>
    <th scope="col">Title</th>
    <th scope="col">Content</th>
  </tr>
  </thead>
  <tbody>
  <?php foreach ($this->data as $value) : ?>
    <tr>
      <td><?php echo $value['id'] ?></td>
      <td style="text-align: center; padding: 30px"><a href="/article/<?php echo $value['id'] ?>"><?php echo $value['title'] ?></a></td>
      <td style="text-align: center; padding: 30px"><?php echo $value['content'] ?></td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>

</body>
</html>