<?php


namespace App\Framework\Http\Serializer;


class NamingStrategy implements NamingStrategyInterface
{

//Да, сериализатор в PHP предназначен для преобразования данных в машинно-читаемый формат и обратно.

//Оба формата, JSON и DTO, относятся к машинно-читаемым форматам.
//DTO (Data Transfer Object) - это шаблон проектирования, который используется для передачи данных между компонентами приложения.
//DTO обычно содержит только данные и не содержит логики или методов.
//JSON (JavaScript Object Notation) - это текстовый формат обмена данными, основанный на языке JavaScript.
//JSON позволяет легко передавать структурированные данные между клиентом и сервером по сети.

//достаю из базы, далее скормливаю сериализации и получаю объект

    /**
     * @param string $name
     *
     * @return string
     */
    public function normalize(string $name): string
    {
        // Преобразование свойства из формата camelCase в формат snake_case
        return strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $name));
    }

    /**
     * @param string $name
     *
     * @return string
     */
    public function denormalize(string $name): string
    {
        // Преобразование свойства из формата snake_case в формат camelCase
        return lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $name))));
    }
}