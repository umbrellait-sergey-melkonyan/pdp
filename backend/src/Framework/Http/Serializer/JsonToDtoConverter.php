<?php


namespace App\Framework\Http\Serializer;

/**
 * Class DependencyContainer
 */
class JsonToDtoConverter
{
    /**
     * @param string $json
     * @param string $class
     * @param array $nameMapping
     * @param NamingStrategyInterface|null $namingStrategy
     *
     * @return object
     */
    function jsonToDto(
        string $json,
        string $class,
        array $nameMapping = [],
        NamingStrategyInterface $namingStrategy = null
    ): object {
        $data = json_decode($json, true);
        if (!is_array($data)) {
            dd('Invalid JSON');
        }

        $dto = new $class();
        $properties = get_class_vars($class);

        foreach ($properties as $property => $value) {
            if (isset($data[$property])) {
                $dtoName = $nameMapping[$property] ?? $property;
                $dto->$dtoName = $value;
            } elseif ($namingStrategy !== null) {
                $snakeCaseProperty = $namingStrategy->denormalize($property);
                if (isset($data[$snakeCaseProperty])) {
                    $dtoName = $nameMapping[$snakeCaseProperty] ?? $snakeCaseProperty;
                    $dto->$dtoName = $value;
                }
            }
        }

        return $dto;
    }
}