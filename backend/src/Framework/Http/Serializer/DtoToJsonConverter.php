<?php


namespace App\Framework\Http\Serializer;


class DtoToJsonConverter
{
    /**
     * @param $dto
     * @param array $nameMapping
     * @param NamingStrategyInterface|null $namingStrategy
     *
     * @return string
     */
    function dtoToJson($dto, array $nameMapping = [], NamingStrategyInterface $namingStrategy = null): string
    {
        $json = [];

        $properties = get_class_vars(get_class($dto));

        foreach ($properties as $property => $value) {
            if ($namingStrategy !== null) {
                $property = $namingStrategy->normalize($property);
            }
            $json[$nameMapping[$property] ?? $property] = $value;
        }

        return json_encode($json);
    }
}