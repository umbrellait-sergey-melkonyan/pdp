<?php


namespace App\Framework\Http\Serializer;


interface NamingStrategyInterface
{
    /**
     * Преобразует имя из PHP-стиля в формат, используемый в JSON.
     *
     * @param string $name
     *
     * @return string
     */
    public function normalize(string $name): string;

    /**
     * Преобразует имя из формата, используемого в JSON, в PHP-стиль.
     *
     * @param string $name
     *
     * @return string
     */
    public function denormalize(string $name): string;
}