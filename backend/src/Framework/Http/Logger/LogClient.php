<?php


namespace App\Framework\Http\Logger;


/**
 * Class LogClient
 */
class LogClient
{
    private LoggerInterface $adapter;

    /**
     * LogClient constructor.
     *
     * @param LoggerInterface $adapter
     */
    public function __construct(LoggerInterface $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @param string $level
     * @param string $message
     * @param array $context
     */
    public function log(string $level, string $message, array $context = []): void
    {
        $this->adapter->log($level, $message, $context);
    }
}