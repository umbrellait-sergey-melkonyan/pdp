<?php
declare(strict_types=1);

namespace App\Framework\Http\Router;

/**
 * Class RouteCollection
 */
class RouteCollection
{
    private array $routes = [];

    /**
     * @param string $name
     * @param        $path
     * @param        $handler
     * @param array  $tokens
     */
    public function any(string $name, $path, $handler, array $tokens = []): void
    {
        $this->addRoute(new RouteData($name, $path, $handler, [], $tokens));
    }

    /**
     * @param string $name
     * @param        $path
     * @param        $handler
     * @param array  $tokens
     */
    public function get(string $name, $path, $handler, array $tokens = []): void
    {
        $this->addRoute(new RouteData($name, $path, $handler, ['GET'], $tokens));
    }

    /**
     * @param string $name
     * @param        $path
     * @param        $handler
     * @param array  $tokens
     */
    public function post(string $name, $path, $handler, array $tokens = []): void
    {
        $this->addRoute(new RouteData($name, $path, $handler, ['POST'], $tokens));
    }

    /**
     * @return array
     */
    public function getRoutes(): array
    {
        return $this->routes;
    }

    /**
     * @param RouteData $data
     */
    public function addRoute(RouteData $data): void
    {
        $this->routes[] = $data;
    }
}