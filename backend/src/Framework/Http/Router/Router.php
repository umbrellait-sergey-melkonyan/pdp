<?php

namespace App\Framework\Http\Router;


use App\Framework\Http\Request\ServerRequestInterface;

/**
 * Class RouteData
 */
class Router implements RouterInterface
{
    private RouteCollection $routes;

    /**
     * Router constructor.
     *
     * @param RouteCollection $routes
     */
    public function __construct(RouteCollection $routes)
    {
        $this->routes = $routes;
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return Result
     */
    public function match(ServerRequestInterface $request): Result
    {

        $path = $request->getUri();
        $method = $request->getMethod();
        $pathSegments = explode('/', $path);
        $pathSegmentsHasId = isset($pathSegments[2]);

        if ($pathSegmentsHasId) {
            $path = '/'.$pathSegments[1] . '/{id}';
        }

        foreach ($this->routes->getRoutes() as $route) {
            if ($route->getMethods() && !in_array($method, $route->getMethods(), true)) {
                continue;
            }

            list($path, $query) = explode('?', $path, 2);
            parse_str($query, $matches);

            $attribute = [];
            foreach ($matches as $key => $value) {
                if (!is_string($key)) {
                    continue;
                }
                $attribute[$key] = $value;
            }

            if ($route->getPath() !== $path) {
                continue;
            }

            if ($pathSegmentsHasId) {
                $attribute['id'] = $pathSegments[2];
            }

            return new Result(
                $route->getName(),
                $route->getHandler(),
                $attribute
            );
        }

        dd('Cant find route');
    }

    public function generate($name, array $params): string
    {
        $arguments = array_filter($params);

        foreach ($this->routes->getRoutes() as $route) {
            if ($name !== $route->getName()) {
                continue;
            }

            $url = preg_replace_callback('~\{([^\}]+)\}~', function ($matches) use (&$arguments) {
                $argument = $matches[1]; // id
                if (!array_key_exists($argument, $arguments)) {
                    dd('missing parameter');
                }

                return $arguments[$argument];
            }, $route->pattern);

            if ($url) {
                return $url;
            }
        }

        dd('Cant find route');
    }

    public function addRoute(RouteData $data): void
    {
        $this->routes->addRoute($data);
    }
}