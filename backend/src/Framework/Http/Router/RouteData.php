<?php

namespace App\Framework\Http\Router;

/**
 * Class RouteData
 */
class RouteData
{
    public $name;
    public $path;
    public $handler;
    public array $methods;
    public array $options;

    /**
     * RouteData constructor.
     *
     * @param       $name
     * @param       $path
     * @param       $handler
     * @param array $methods
     * @param array $options
     */
    public function __construct($name, $path, $handler, array $methods, array $options)
    {
        $this->name = $name;
        $this->path = $path;
        $this->handler = $handler;
        $this->methods = array_map('mb_strtoupper', $methods);
        $this->options = $options;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return mixed
     */
    public function getHandler()
    {
        return $this->handler;
    }

    /**
     * @return array
     */
    public function getMethods(): array
    {
        return $this->methods;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }
}
