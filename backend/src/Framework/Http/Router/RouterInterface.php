<?php

namespace App\Framework\Http\Router;

use App\Framework\Http\Request\ServerRequestInterface;

interface RouterInterface
{
    /**
     * @param ServerRequestInterface $request
     *
     * @return Result
     */
    public function match(ServerRequestInterface $request): Result;

    /**
     * @param $name
     * @param array $params
     *
     * @return string
     */
    public function generate($name, array $params): string;

    /**
     * @param RouteData $data
     */
    public function addRoute(RouteData $data): void;
}
