<?php

namespace App\Framework\Http;

use Psr\Http\Message\ResponseInterface;

/**
 * Class ResponseSender
 */
class ResponseSender
{
    public function send(ResponseInterface $response): void
    {
        header(sprintf(
            'HTTP/%s %d %s',
            $response->getProtocolVersion(),
            $response->getStatusCode(),
            $response->getReasonPhrase()
        ));
        foreach ($response->getHeaders() as $name => $value) {
                header(sprintf('%s: %s', $name, $value));
        }

        echo $response->getBody();
    }
}
