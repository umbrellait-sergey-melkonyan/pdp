<?php
declare(strict_types=1);

namespace App\Framework\Http\Responses;

use App\Framework\Http\Stream;

/**
 * Class JsonResponse
 */
class JsonResponse extends Response
{
    // implements JsonSerializable
    const DEFAULT_JSON_FLAGS = 79;

    private $payload;

    /**
     * @var int
     */
    private int $encodingOptions;

    /**
     * JsonResponse constructor.
     *
     * @param           $data
     * @param int       $status
     * @param array     $headers
     * @param int       $encodingOptions
     */
    public function __construct(
        $data,
        int $status = 200,
        array $headers = [],
        int $encodingOptions = self::DEFAULT_JSON_FLAGS
    )
    {
        $this->setPayload($data);
        $this->encodingOptions = $encodingOptions;
        $headers = $this->injectContentType('application/json', $headers);
        $json = $this->jsonEncode($data, $this->encodingOptions);
        $body = $this->createBodyFromJson($json);

        parent::__construct((string)$body, $status, $headers);
    }

    private function createBodyFromJson(string $json): Stream
    {
        $body = new Stream('php://temp', 'wb+');
        $body->write($json);
        $body->rewind();

        return $body;
    }

    /**
     * @param $data
     * @param int $encodingOptions
     *
     * @return string
     */
    private function jsonEncode($data, int $encodingOptions): string
    {
        if (is_resource($data)) {
            dd('Cannot JSON encode resources');
        }

        json_encode(null);

        $json = json_encode($data, $encodingOptions);

        if (JSON_ERROR_NONE !== json_last_error()) {
            dd(sprintf(
                'Unable to encode data to JSON in %s: %s',
                __CLASS__,
                json_last_error_msg()
            ));
        }

        return $json;
    }

    /**
     * @param mixed $data
     */
    private function setPayload($data): void
    {
        if (is_object($data)) {
            $data = clone $data;
        }

        $this->payload = $data;
    }
}