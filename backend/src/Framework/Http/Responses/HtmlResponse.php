<?php
declare(strict_types=1);

namespace App\Framework\Http\Responses;

use App\Framework\Http\Stream;
use Psr\Http\Message\StreamInterface;

/**
 * Class HtmlResponse
 */
class HtmlResponse extends Response
{
    /**
     * HtmlResponse constructor.
     *
     * @param           $html
     * @param int       $status
     * @param array     $headers
     */
    public function __construct($html, int $status = 200, array $headers = [])
    {
        parent::__construct(
            $this->createBody($html),
            $status,
            $this->injectContentType('text/html; charset=utf-8', $headers)
        );
    }

    /**
     * @param $html
     *
     * @return StreamInterface
     */
    private function createBody($html): StreamInterface
    {
        if ($html instanceof StreamInterface) {
            return $html;
        }

        if (!is_string($html)) {
            dd(sprintf(
                'Invalid content (%s) provided to %s',
                (is_object($html) ? get_class($html) : gettype($html)),
                __CLASS__
            ));
        }

        $body = new Stream('php://temp', 'wb+');
        $body->write($html);
        $body->rewind();

        return $body;
    }
}