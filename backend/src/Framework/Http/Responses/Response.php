<?php

namespace App\Framework\Http\Responses;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

/**
 * Class Response
 */
class Response implements ResponseInterface
{
    public const HTTP_OK = 200;
    public const HTTP_MOVED_PERMANENTLY = 301;
    public const HTTP_BAD_REQUEST = 400;
    public const HTTP_FORBIDDEN = 403;
    public const HTTP_NOT_FOUND = 404;
    public const HTTP_INTERNAL_SERVER_ERROR = 500;

    protected array $headers = [];

    /**
     * @var string
     */
    protected string $content;

    /**
     * @var string
     */
    protected string $version;

    /**
     * @var int
     */
    protected int $statusCode;

    /**
     * @var string
     */
    protected string $statusText = '';

    /**
     * @var array
     */
    public static array $statuses = [
        self::HTTP_OK => 'OK',
        self::HTTP_MOVED_PERMANENTLY => 'Moved Permanently',
        self::HTTP_BAD_REQUEST => 'Bad Request',
        self::HTTP_FORBIDDEN => 'Forbidden',
        self::HTTP_NOT_FOUND => 'Not Found',
        self::HTTP_INTERNAL_SERVER_ERROR => 'Internal Server Error',
    ];


    /**
     * Response constructor.
     *
     * @param string|StreamInterface $content
     * @param int                    $status
     * @param array                  $headers
     */
    public function __construct($content = '', int $status = 200, array $headers = [])
    {
        $this->content = $content;
        $this->statusCode = $status;
        $this->setHeaders($headers);
        $this->withProtocolVersion('1.0');
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->content;
    }

    /**
     * @param StreamInterface $content
     *
     * @return $this
     */
    public function withBody(StreamInterface $content): self
    {
        $newContent = clone $this;
        $this->content = $newContent;

        return $newContent;
    }

    /**
     * @param string $version
     *
     * @return $this
     */
    public function withProtocolVersion($version): Response
    {
        $this->version = $version;

        return $this;
    }

    /**
     * @return string
     */
    public function getProtocolVersion(): string
    {
        return $this->version;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @return string
     */
    public function getReasonPhrase(): string
    {
        if (!$this->statusText && isset(self::$statuses[$this->statusCode])) {
            $this->statusText = self::$statuses[$this->statusCode];
        }

        return $this->statusText;
    }

    /**
     * @param int $code
     * @param string $reasonPhrase
     *
     * @return Response
     */
    public function withStatus($code, $reasonPhrase = ''): self
    {
        $newStatus = clone $this;
        $newStatus->statusCode = $code;
        $newStatus->statusText = $reasonPhrase;

        return $newStatus;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param array $headers
     *
     * @return Response
     */
    public function setHeaders(array $headers): self
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * @param $name
     *
     * @return bool
     */
    public function hasHeader($name): bool
    {
        return isset($this->headers[$name]);
    }

    /**
     * @param $name
     *
     * @return mixed|null
     */
    public function getHeader($name)
    {
        if (!$this->hasHeader($name)) {
            return null;
        }

        return $this->headers[$name];
    }

    /**
     * @param $name
     * @param $value
     *
     * @return Response
     */
    public function withHeader($name, $value): self
    {
        $newHeader = clone $this;

        if ($newHeader->hasHeader($name)) {
            unset($newHeader->headers[$name]);
        }

        $newHeader->headers[$name] = $value;

        return $newHeader;
    }

    /**
     * @param string    $contentType
     * @param array     $headers
     *
     * @return array
     */
    public function injectContentType(string $contentType, array $headers): array
    {
        $hasContentType = array_reduce(array_keys($headers), function ($carry, $item) {
            return $carry ?: (strtolower($item) === 'content-type');
        }, false);

        if (!$hasContentType) {
            $headers['content-type'] = [$contentType];
        }

        return $headers;
    }

    public function getHeaderLine($name)
    {
        // TODO: Implement getHeaderLine() method.
    }

    public function withAddedHeader($name, $value)
    {
        // TODO: Implement withAddedHeader() method.
    }

    public function withoutHeader($name)
    {
        // TODO: Implement withoutHeader() method.
    }
}