<?php


namespace App\Framework\Http\Di;

use App\Framework\Http\Di\Exceptions\NotInstantiableException;
use App\Framework\Http\Di\Exceptions\UnresolvableDependencyException;
use Psr\Container\ContainerInterface;
use ReflectionClass;

/**
 * Class DependencyContainer
 */
class DependencyContainer implements ContainerInterface
{
    private array $services = [];
    private array $serviceNames = [];

//имплементы или трейты

    /**
     * @param string $id
     *
     * @return mixed
     *
     * @throws
     */
    public function get(string $id): object
    {
        if ($this->has($id)) {
            return $this->services[$id];
        }

        $dependencyInstance = $this->autowire($id);
        $this->set($id, $dependencyInstance);

        return $dependencyInstance;
    }

    /**
     * @param string $id
     *
     * @return bool
     */
    public function has(string $id): bool
    {
        return array_key_exists($id, $this->services);
    }

    /**
     * @param string $id
     * @param $service
     */
    public function set(string $id, $service)
    {
        $this->services[$id] = $service;
    }

    /**
     * @param string $className
     *
     * @return mixed|object
     *
     * @throws
     */
    public function autowire(string $className)
    {
        $reflector = new ReflectionClass($className);

        if (!$reflector->isInstantiable()) {
            throw new NotInstantiableException($className);
        }

        $constructor = $reflector->getConstructor();

        if (is_null($constructor)) {
            return new $className();
        }

        $parameters = $constructor->getParameters();
        $dependencies = [];

        foreach ($parameters as $parameter) {
            $dependency = $parameter->getType();

            if (!$dependency) {
                continue;
            }

            if ($dependency->isBuiltin()) {
                if ($parameter->isDefaultValueAvailable()) {
                    $dependencies[] = $parameter->getDefaultValue();
                } else {
                    throw new UnresolvableDependencyException($className, $parameter->getName());
                }
            } else {
                if (in_array($dependency->getName(), $this->serviceNames)) {
                    dd('Бро ты пытаешься сломать мой di');
                }
                $this->serviceNames[] = $dependency->getName();
                $dependencies[] = $this->get($dependency->getName());
            }
        }

        return $reflector->newInstanceArgs($dependencies);
    }
}