<?php

namespace App\Framework\Http\Di\Exceptions;

use RuntimeException;

/**
 * Class NotInstantiableException
 */
class NotInstantiableException extends RuntimeException
{
    public function __construct(string $message = "", int $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
