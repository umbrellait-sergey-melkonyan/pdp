<?php

namespace App\Framework\Http\Di\Exceptions;

use Psr\Container\ContainerExceptionInterface;
use RuntimeException;

/**
 * Class UnresolvableDependencyException
 */
class UnresolvableDependencyException extends RuntimeException implements ContainerExceptionInterface
{
    private string $className;
    private string $parameterName;

    public function __construct(string $className, string $parameterName)
    {
        $this->className = $className;
        $this->parameterName = $parameterName;

        parent::__construct("Unresolvable dependency found for parameter '{$parameterName}' in class '{$className}'");
    }

    /**
     * @return string
     */
    public function getClassName(): string
    {
        return $this->className;
    }

    /**
     * @return string
     */
    public function getParameterName(): string
    {
        return $this->parameterName;
    }
}