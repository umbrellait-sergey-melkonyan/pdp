<?php

namespace App\Framework\Http;

use App\Framework\Http\Request\Uri;
use App\Framework\Http\Request\UriInterface;

/**
 * Class RequestFactory
 */
class RequestFactory
{
    private string $base_url;

    /**
     * RequestFactory constructor.
     *
     * @param string $base_url
     */
    public function __construct(string $base_url)
    {
        $this->base_url = $base_url;
    }

    /**
     * @param string $method
     * @param UriInterface $path
     * @param null $headers
     * @param null $params
     * @param null $auth
     *
     * @return Request
     */
    public function get(string $method, UriInterface $path, $headers = null, $params = null, $auth = null): Request
    {
        return new Request($method, $path);
    }

    /**
     * @param string $method
     * @param UriInterface $path
     * @param null $headers
     * @param null $data
     * @param null $json
     * @param null $auth
     *
     * @return Request
     */
    public function post(string $method, UriInterface $path, $headers = null, $data = null, $json = null, $auth = null): Request
    {
        return new Request($method, $path);
    }

    /**
     * @param string $method
     * @param UriInterface $path
     * @param null $headers
     * @param null $data
     * @param null $json
     * @param null $auth
     *
     * @return Request
     */
    public function put(string $method, UriInterface $path, $headers = null, $data = null, $json = null, $auth = null): Request
    {
        return new Request($method, $path);
    }

    /**
     * @param string $method
     * @param UriInterface $path
     * @param null $headers
     * @param null $data
     * @param null $json
     * @param null $auth
     *
     * @return Request
     */
    public function delete(string $method, UriInterface $path, $headers = null, $data = null, $json = null, $auth = null): Request
    {
        return new Request($method, $path);
    }

    /**
     * @return Request
     */
    public static function fromGlobals(): Request
    {
        $path = $_SERVER['REQUEST_URI'];
        $method = $_SERVER['REQUEST_METHOD'];
        $headers = getallheaders();
        $params = $_GET;
        $data = $_POST;

        $factory = new RequestFactory($_SERVER['HTTP_HOST']);
        $name = strtolower($method);

        if ($method === 'GET') {
            return $factory->$name($method, new Uri($path), $headers, $params);
        }

        return $factory->$name($method, new Uri($path), $headers, $data);
    }
}