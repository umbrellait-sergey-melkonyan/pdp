<?php
declare(strict_types=1);

namespace App\Framework\Http;

use Psr\Http\Message\StreamInterface;

/**
 * Class Stream
 */
class Stream implements StreamInterface
{
    protected $resource;

    protected $stream;

    public function __construct($stream, string $mode = 'r')
    {
        $this->setStream($stream, $mode);
    }

    public function __toString()
    {
        if (!$this->isReadable()) {
            return '';
        }

        if ($this->isSeekable()) {
            $this->rewind();

            return $this->getContents();
        }

        return '';
    }

    public function close()
    {
        // TODO: Implement close() method.
    }

    public function detach()
    {
        // TODO: Implement detach() method.
    }

    public function getSize()
    {
        // TODO: Implement getSize() method.
    }

    public function tell()
    {
        // TODO: Implement tell() method.
    }

    public function eof()
    {
        // TODO: Implement eof() method.
    }

    public function isSeekable(): bool
    {
        if (!$this->resource) {
            return false;
        }

        $meta = stream_get_meta_data($this->resource);

        return $meta['seekable'];
    }

    /**
     * @param int $offset
     * @param int $whence
     */
    public function seek($offset, $whence = SEEK_SET)
    {
        if (!$this->resource) {
            dd('dont have resource');
        }

        if (!$this->isSeekable()) {
            dd('resource is not seekable');
        }

        $result = fseek($this->resource, $offset, $whence);

        if (0 !== $result) {
            dd('result is false');
        }
    }

    public function rewind(): void
    {
        $this->seek(0);
    }

    /**
     * @return bool
     */
    public function isWritable(): bool
    {
        if (!$this->resource) {
            return false;
        }

        $meta = stream_get_meta_data($this->resource);
        $mode = $meta['mode'];

        return (
            strstr($mode, 'x')
            || strstr($mode, 'w')
            || strstr($mode, 'c')
            || strstr($mode, 'a')
            || strstr($mode, '+')
        );
    }

    /**
     * @param string $string
     *
     * @return false|int|mixed
     */
    public function write($string)
    {
        if (!$this->resource) {
            dd('dont have resource');
        }

        if (!$this->isWritable()) {
            dd('resource is not writable');

        }

        $result = fwrite($this->resource, $string);

        if (false === $result) {
            dd('result is false');
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function isReadable(): bool
    {
        if (!$this->resource) {
            return false;
        }

        $meta = stream_get_meta_data($this->resource);
        $mode = $meta['mode'];

        return (strstr($mode, 'r') || strstr($mode, '+'));
    }

    public function read($length)
    {
        // TODO: Implement read() method.
    }

    public function getContents()
    {
        if (!$this->isReadable()) {
            dd('dont isReadable');
        }

        $result = stream_get_contents($this->resource);

        if (false === $result) {
            dd('result is false');
        }

        return $result;
    }

    public function getMetadata($key = null)
    {
        // TODO: Implement getMetadata() method.
    }

    /**
     * @param $stream
     * @param string $mode
     */
    private function setStream($stream, string $mode = 'r'): void
    {
        $error = null;
        $resource = $stream;

        if (is_string($stream)) {
            set_error_handler(function ($e) use (&$error) {
                if ($e !== E_WARNING) {
                    return;
                }

                $error = $e;
            });
            $resource = fopen($stream, $mode);
            restore_error_handler();
        }

        if ($error) {
            dd('Invalid stream reference provided');
        }

        if (!is_resource($resource) || 'stream' !== get_resource_type($resource)) {
            dd('Invalid stream provided; must be a string stream identifier or stream resource');
        }

        if ($stream !== $resource) {
            $this->stream = $stream;
        }

        $this->resource = $resource;
    }
}