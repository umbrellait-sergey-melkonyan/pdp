<?php


namespace App\Framework\Http\ORM;

use PDO;
use PDOException;
use PDOStatement;

/**
 * Class Database
 */
class Database
{
    protected $db;

    public function __construct() {
        try {
            $config = (include __DIR__ . '/../../../../config/config.php')['db'];
            $this->db = new PDO(
                'mysql:host=' . $config['host'] . ';dbname=' . $config['dbname'],
                $config['user'],
                $config['password']);
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
            die();
        }
    }

    /**
     * @param $sql
     * @param array $params
     *
     * @return false|PDOStatement
     */
    public function query($sql, $params = [])
    {
        $stmt = $this->db->prepare($sql);
        $stmt->execute($params);

        return $stmt;
    }

    /**
     * @return bool
     */
    public function startTransaction(): bool
    {
        return $this->db->beginTransaction();
    }

    /**
     * @return bool
     */
    public function commit(): bool
    {
        return $this->db->commit();
    }

    /**
     * @return bool
     */
    public function rollback(): bool
    {
        return $this->db->rollback();
    }

    /**
     * @return string
     */
    public function lastInsertId(): string
    {
        return $this->db->lastInsertId();
    }

    /**
     * @param string $sql
     * @param $values
     *
     * @return bool
     */
    public function execute(string $sql, $values): bool
    {
        $stmt = $this->db->prepare($sql);

        return $stmt->execute($values);
    }
}