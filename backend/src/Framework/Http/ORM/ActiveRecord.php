<?php


namespace App\Framework\Http\ORM;

/**
 * Class ActiveRecord
 */
class ActiveRecord
{
//    trait


//в релайшнс должны лежать функция по которой аргкментам которой нужно понимать что откуда доставать и в каком количестве

    protected $db;
    protected string $table = '';
    protected string $primaryKey = '';
    protected array $attributes = [];
    protected array $relations = [];
    protected array $originalAttributes = [];

    public function __construct(string $table, array $relations = [], array $attributes = [])
    {
        $this->db = new Database;
        $this->table = $table;
        $this->relations = array_merge(...$relations);
        $this->attributes = $attributes;
        $this->originalAttributes = $attributes;
    }

    /**
     * @param string $name
     *
     * @return mixed|null
     */
    public function __get(string $name)
    {

//        [
//            'author' => ['Users', 'ManyToOne', 'author_id'],
//            'author' => ['Users', 'OneToOne', 'user_id', true],
//            'author' => ['Users', 'OneToOne', 'article_id', false],
//            'authors' => ['Users', 'OneToMany', 'article_id'],
//            'authors' => ['Users', 'ManyToMany', 'article_id', 'user_id', 'users_articles'],
//        ];

        if (array_key_exists($name, $this->relations)) {
            $relationData = $this->relations[$name];
            if (in_array('ManyToOne', $relationData)) {
                if (array_key_exists($relationData[2], $this->attributes)) {
                    $id = $this->attributes[$relationData[2]];

                    if (!$id) {
                        return null;
                    }

                    return $this->find(['id' => $id], null, null, $relationData[0])[0];
                } else {
                    return new \Exception('такой такого атрибута');
                }
            } elseif (in_array('OneToMany', $relationData)) {
                $id = $this->attributes['id'];

                if (!$id) {
                    return null;
                }

                return $this->find([$relationData[2] => $id], null, null, $relationData[0]);
            } elseif (in_array('ManyToMany', $relationData)) {
                $id = $this->attributes['id'];

                if (!$id) {
                    return null;
                }

                $data = $this->find([$relationData[2] => $id],null, null, $relationData[4]);

                $ids = [];
                foreach ($data as $datum) {
                    $ids[] = $datum[$relationData[3]];
                }

                return $this->getByIds($ids, $relationData[0]);
            } elseif (in_array('OneToOne', $relationData)) {
                if (array_key_exists($relationData[2], $this->attributes)) {
                    if ($relationData[3]) {
                        $id = $this->attributes[$relationData[2]];

                        if (!$id) {
                            return null;
                        }

                        return $this->find(['id' => $id], null, null, $relationData[0])[0];
                    } else {
                        $id = $this->attributes['id'];

                        if (!$id) {
                            return null;
                        }

                        return $this->find([$relationData[2] => $id], null, null, $relationData[0])[0];
                    }

                }
            }
        }

        if (array_key_exists($name, $this->attributes)) {
            return $this->attributes[$name];
        }

        return new \Exception('такой такого атрибута');
    }

    /**
     * @param string $name
     * @param $value
     */
    public function __set(string $name, $value)
    {
        $this->attributes[$name] = $value;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }


    public function setAttribute(string $key, $value): void
    {
        if (is_numeric($key)) {
            return;
        }
        $this->attributes[$key] = $value;
    }

    /**
     * @param string $key
     * @param $value
     */
    public function setOriginalAttribute(string $key, $value): void
    {
        if (is_numeric($key)) {
            return;
        }
        $this->originalAttributes[$key] = $value;
    }

    /**
     * @return array
     */
    public function getOriginalAttributes(): array
    {
        return $this->originalAttributes;
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        if ($this->isNewRecord()) {
            return $this->insert();
        }

        return $this->update();
    }

    /**
     * @return bool
     */
    public function delete(): bool
    {
        $sql = "DELETE FROM {$this->table} WHERE {$this->primaryKey} = ?";
        $params = [$this->attributes[$this->primaryKey]];

        return $this->db->execute($sql, $params);
    }

    /**
     * @return bool
     */
    public function isNewRecord(): bool
    {
        return empty($this->attributes[$this->primaryKey]);
    }

    /**
     * @return bool
     */
    public function insert(): bool
    {
        $sql = "INSERT INTO {$this->table} (";
        $placeholders = '';
        $values = [];
        foreach ($this->attributes as $key => $value) {
            if ($key === $this->primaryKey || $value === null) {
                continue;
            }
            $sql .= "$key, ";
            $placeholders .= '?, ';
            $values[] = $value;
        }

        $sql = rtrim($sql, ', ') . ') VALUES (' . rtrim($placeholders, ', ') . ')';
        $result = $this->db->execute($sql, $values);

        if ($result) {
            $this->attributes[$this->primaryKey] = $this->db->lastInsertId();
            $this->originalAttributes = $this->attributes;
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function update(): bool
    {
        $sql = "UPDATE {$this->table} SET ";
        $values = [];

        foreach ($this->attributes as $key => $value) {
            if ($key === $this->primaryKey || $value === null || $value === $this->originalAttributes[$key]) {
                continue;
            }
            $sql .= "$key = ?, ";
            $values[] = $value;
        }

        $sql = rtrim($sql, ', ') . " WHERE {$this->primaryKey} = ?";
        $values[] = $this->attributes[$this->primaryKey];
        $result = $this->db->execute($sql, $values);

        if ($result) {
            $this->originalAttributes = $this->attributes;
        }

        return $result;
    }

    /**
     * @param array $conditions
     * @param int|null $limit
     * @param int|null $offset
     * @param string|null $tableName
     *
     * @return array
     */
    public function find(
        array $conditions = [],
        int $limit = null,
        int $offset = null,
        ?string $tableName = null
    ): array {
        $table = $this->table;
        if ($tableName) {
            $table = $tableName;
        }

        $sql = "SELECT * FROM {$table}";
        $where = null;
        $params = [];
        if ($conditions) {
            $where = ' WHERE ';
            foreach ($conditions as $key => $value) {
                $where .= $key . ' = ? AND ';
                $params[] = $value;
            }
            $where = rtrim($where, ' AND ');
        }

        if ($where) {
            $sql .= $where;
        }

        if ($limit) {
            $sql .= ' LIMIT ' . (int) $limit;
        }

        if ($offset) {
            $sql .= ' OFFSET ' . (int) $offset;
        }

        $result = $this->db->query($sql, $params);

        $objects = [];
        foreach ($result as $row) {
            $object = new static();
            //правильно создавать каждый раз заново
            foreach ($row as $key => $value) {
                $object->setAttribute($key, $value);
                $object->setOriginalAttribute($key, $value);
            }

            $objects[] = $object;
        }

        return $objects;
    }

    /**
     * @param array $ids
     * @param string|null $tableName
     *
     * @return array
     */
    private function getByIds(array $ids, ?string $tableName = null): array
    {
        $table = $this->table;
        if ($tableName) {
            $table = $tableName;
        }

        $idStr = implode(',', array_map(function ($id) {
            return (int) $id;
        }, $ids));

        $sql = "SELECT * FROM {$table} WHERE `id` IN ({$idStr})";
        $result = $this->db->query($sql);

        $objects = [];
        foreach ($result as $row) {
            $object = new static();
            foreach ($row as $key => $value) {
                $object->setAttribute($key, $value);
                $object->setOriginalAttribute($key, $value);
            }

            $objects[] = $object;
        }

        return $objects;
    }
}