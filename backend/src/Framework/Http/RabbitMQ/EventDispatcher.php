<?php

namespace App\Framework\Http\RabbitMQ;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Class EventDispatcher
 */
class EventDispatcher
{
    private AMQPStreamConnection $connection;
    private AMQPChannel $channel;
    private string $exchangeName;

    /**
     * EventDispatcher constructor.
     *
     * @param string $exchangeName
     */
    public function __construct($exchangeName = 'events')
    {
        $this->connection = new AMQPStreamConnection('localhost', 5672, 'root', 'root');
        $this->channel = $this->connection->channel();
        $this->exchangeName = $exchangeName;
        // не знаю fanout это правильный exchange
        $this->channel->exchange_declare($this->exchangeName, 'fanout', false, false, false);
    }

    /**
     * @param $event
     * @param $data
     */
    public function dispatchEvent($event, $data): void
    {
        $message = new AMQPMessage(json_encode(['event' => $event, 'data' => $data]));

        $this->channel->basic_publish($message, $this->exchangeName);
    }

    //посмотрел на стековерфло вроде должно сработать
    public function __destruct()
    {
        $this->channel->close();
        $this->connection->close();
    }
}