<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';

use App\Framework\Http\RabbitMQ\Consumer;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

// Подключение к RabbitMQ
$connection = new AMQPStreamConnection('localhost', 5672, 'root', 'root');
$channel = $connection->channel();

// Определение имени очереди
$queueName = Consumer::QUEUE_NAME;

$message = new AMQPMessage(json_encode(['event' => Consumer::EVENT, 'data' => ['user_id' => 123, 'lastname' => 'Sergo']]));
$channel->basic_publish($message, '', $queueName);
//event_calaboration паттерн
//взаимодействия с евентами


echo "Сообщение успешно отправлено.";

$channel->close();
$connection->close();


//php producer.php
//php client.php