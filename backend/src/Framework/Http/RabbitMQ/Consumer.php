<?php

namespace App\Framework\Http\RabbitMQ;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Class EventDispatcher
 */
class Consumer
{
    public const QUEUE_NAME = 'event_new_user';
    public const EVENT = 'user_registered';

    /**
     * @var AMQPChannel
     */
    private AMQPChannel $channel;

    /**
     * @var string
     */
    private string $queue;

    /**
     * @var AMQPStreamConnection
     */
    private AMQPStreamConnection $connection;

    /**
     * Consumer constructor.
     *
     * @param AMQPStreamConnection  $connection
     * @param AMQPChannel           $channel
     * @param string                $queue
     */
    public function __construct(AMQPStreamConnection $connection, AMQPChannel $channel, string $queue)
    {
        $this->connection = $connection;
        $this->channel = $channel;
        $this->queue = $queue;
    }

    public function startConsuming(): void
    {
        while ($message = $this->channel->basic_get($this->queue)) {
            $messageData = json_decode($message->getBody(), true);
            $eventType = $messageData['event'];
            $eventData = $messageData['data'];

            switch ($eventType) {
                case self::EVENT:
                    $this->handleUserRegisteredEvent($eventData);
                    break;
            }

            // Подтверждение обработки сообщения
            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);

            echo "Событие обработано: $eventType" . PHP_EOL;
        }
    }

    public function isConsuming(): bool
    {
        return (bool) $this->connection;
    }


    public function stopConsuming(): void
    {
        $this->channel->basic_cancel($this->queue);
    }

    /**
     * @param array $eventData
     */
    private function handleUserRegisteredEvent(array $eventData): void
    {
        $userId = $eventData['user_id'];
        $lastname = $eventData['name'];

        echo "Получено событие user_registered. User ID: $userId, Lastname: $lastname" . PHP_EOL;
    }
}
