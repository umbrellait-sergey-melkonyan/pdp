<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';

use App\Framework\Http\RabbitMQ\Consumer;
use PhpAmqpLib\Connection\AMQPStreamConnection;

$connection = new AMQPStreamConnection('localhost', 5672, 'root', 'root');
$channel = $connection->channel();

$queueName = Consumer::QUEUE_NAME;
$channel->queue_declare($queueName, false, true, false, false);

$consumer = new Consumer($connection, $channel, $queueName);

$consumer->startConsuming();

if ($consumer->isConsuming()) {
    echo 'Consumer is consuming messages.';
} else {
    echo 'Consumer is not consuming messages.';
}

$consumer->stopConsuming();
$channel->close();
$connection->close();


