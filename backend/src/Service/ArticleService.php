<?php


namespace App\Service;

use App\Models\Article;

/**
 * Class ArticleService
 */
class ArticleService
{
    /**
     * @return array[]
     */
    public function getData(): array
    {
        return (new Article())->findAll();
    }
}