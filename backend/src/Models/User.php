<?php

namespace App\Models;

use App\Connection;
use App\Model;

/**
 * Class User
 */
class User extends Model
{
    public const TABLE = 'users';

    public string $email;
    public string $name;
    public string $password;

    /**
     * User constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        foreach ($data as $key => $datum) {
            $this->$key = $datum;
        }
    }

    /**
     * @param string $email
     *
     * @return User
     */
    public static function getByEmail(string $email): self
    {
        $db = new Connection();

        return $db->getByEmail($email, 'SELECT * FROM ' . self::TABLE . ' WHERE email = ?', static::class);
    }

//    public function new()
//    {
//        return $this->belongsTo(Article::class);
//    }
}