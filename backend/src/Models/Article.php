<?php

namespace App\Models;

use App\Framework\Http\ORM\ActiveRecord;

/**
 * Class Article
 */
class Article extends ActiveRecord
{
    public const TABLE = 'news';

    private array $data;


    // при вызове $article->title - данные берутся из атрибутов с помошью магических методов
    // при вызове $article->content - данные берутся из атрибутов с помошью магических методов
    // при вызове $article->author - данные берутся из базы

    //laravel елоквиент

    /**
     * Article constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        foreach ($data as $key => $datum) {
            $this->$key = $datum;
        }

        $this->data = $data;

        parent::__construct(self::TABLE, ['author' => ['Users', 'ManyToOne', 'author_id']]);
    }

}