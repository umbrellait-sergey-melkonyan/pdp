<?php
declare(strict_types=1);

use App\Framework\Http\Di\DependencyContainer;
use App\Framework\Http\Logger\FileLogger;
use App\Framework\Http\Logger\LogClient;
use App\Framework\Http\RequestFactory;
use App\Framework\Http\Responses\JsonResponse;
use App\Framework\Http\Responses\Response;
use App\Framework\Http\ResponseSender;
use App\Framework\Http\Router\RouteCollection;
use App\Framework\Http\Router\Router;
use App\Models\User;
use Firebase\JWT\JWT;

require_once dirname(__DIR__) . '/vendor/autoload.php';
session_start();
// Initialization

$routes = new RouteCollection();

$config = (include __DIR__ . '/../config/config.php');

if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) {
    $secret_key = $config['secret_key'];

    try {
        $decoded = JWT::decode($_COOKIE['token'], [base64_encode(sodium_crypto_sign_publickey($secret_key))]);
    } catch (SodiumException $e) {
        dd("Пошел вон мошенник");
    }

    $username = $decoded->username;

    if (!$username || (isset($_SESSION['username']) && $_SESSION['username'] !== $username)) {
        dd("Пошел вон мошенник");
    }

    $user = User::getByEmail($username);

    if (!$user) {
        dd("Пошел вон мошенник");
    }

    $routes->get('home', '/', 'DataController@action', ['token' => $_COOKIE['token']]);
    $routes->get('logout', '/logout', 'AuthController@logout', ['token' => $_COOKIE['token']]);
    $routes->get('about', '/about', 'HelloWorld@action', ['token' => $_COOKIE['token']]);
    $routes->get('article', '/article', 'ArticleController@getListAction', ['token' => $_COOKIE['token']]);
    $routes->get('article_show', '/article/{id}', 'ArticleController@getArticleById', ['token' => $_COOKIE['token']]);
} else {
    $routes->get('sing up', '/', 'AuthController@singUp');
    $routes->get('singUp', '/login', 'AuthController@singUp');
    $routes->post('login', '/login', 'AuthController@login');
}

$router = new Router($routes);

//di

$container = new DependencyContainer();

//функция которая будет вызывать объект когда он потребуется

// log

$logFileAdapter = new FileLogger();
$logClient = new LogClient($logFileAdapter);

//  Running

$request = RequestFactory::fromGlobals();

try {
    $result = $router->match($request);

    foreach ($result->getAttributes() as $attribute => $value) {
        $request = $request->withAttribute($attribute, $value);
    }

    $handler = $result->getHandler();

    $controllerAction = explode('@', $handler);

    $controller = '\App\Controller\\' . $controllerAction[0];
    $action = $controllerAction[1];

    $controllerInstance = $container->get($controller);
    $response = $controllerInstance->$action($request);
} catch (LogicException $exception) {
    $response = new JsonResponse(['error' => 'Undefined page3'], Response::HTTP_NOT_FOUND);
}

/// Google

//$client = new Google_Client();
//$client->setClientId($config['google']['client_Id']);
//$client->setClientSecret($config['google']['client_secret']);
//$client->setRedirectUri('http://localhost.de/');
//$client->setScopes(array('email', 'profile'));
//
//if (!isset($_GET['code'])) {
//    $auth_url = $client->createAuthUrl();
//    header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
//    exit;
//}
//
//$token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
//dd($client);
//$oauth2 = new Google_Service_Oauth2($client);
//$user = $oauth2->userinfo->get();
// по идее дальше логиню по емаил
//
// PreProcessing

if ($request->getHeader('Content-Type') && preg_match('#json#i', $request->getHeader('Content-Type'))) {
    $request = $request->withParsedBody(json_decode($request->getBody()));
}
if ($response) {
// PostProcessing
    $response = $response->withHeader('X-Developer', 'Sergio');

    // Sending
    $emitter = new ResponseSender();
    $emitter->send($response);
}




